'use strict';
const express = require('express');
const morgan = require('morgan');
const app = express();
app.use(morgan('dev'));

var todos = [
    { id: 1, name: 'surya' },
    { id: 2, name: 'murali' },
    { id: 3, name: 'rajesh' }
];
var count = todos.length;

app.post('/', (req, res) => {
    var newTodo = JSON.parse(req.body);
    count = count + 1;
    newTodo.id = count;
    todos.add(newTodo);
    res.status(200).json();
});
app.get('/', (req, res) => {
    res.status(201).json(todos);
});
app.put('/Todos/:id', (req, res) => {
    var id = req.params.id;
    var updatedTodo = JSON.parse(req.body);
    updatedTodo.id = parseInt(id);
    todos.update(updatedTodo, (err, data) => {
        if (err) {
            res.status(404, 'the todo not found').send();
        } else {
            res.status(204).send(data);
        }
    });
});
app.delete('/Todos/:id', (req, res) => {
    var id = parseInt(req.params.id);
    if (todos.filter(todo => todo.id !== id)) {
        res.status(200).send();
    } else {
        res.status(404).send();
    }
});

app.listen(60703, () => console.log('ready...'));